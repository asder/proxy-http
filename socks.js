const net = require('net');
const socks = require('socks5');

const server = socks.createServer((userSocket, port, address, proxy_ready) => {
	const remoteSocket = net.createConnection({port, host: address}, proxy_ready);
	
    remoteSocket.on('data', (chunk) => {
        if (!userSocket.write(chunk)) {
			remoteSocket.pause();
			setTimeout(() => {
				remoteSocket.resume();    
			}, 100);
		}
	});

	userSocket.on('data', (chunk) => {
		if (!remoteSocket.write(chunk)) {
			userSocket.pause();
			setTimeout(() => {
				userSocket.resume();    
			}, 100);
		}
	});
	
	userSocket.on('drain', () => {
		remoteSocket.resume();
	});

	remoteSocket.on('drain', () => {
		userSocket.resume();
	});
	
	remoteSocket.on('error', (e) => {
	});

	userSocket.on('error', (e) => {
	});

	remoteSocket.on('close', () => {
		userSocket.end();
	});
	
	userSocket.on('close', () => {
		remoteSocket.removeAllListeners('data');
		remoteSocket.end();
	});

}, process.argv[3] && process.argv[4] && {username: process.argv[3], password:process.argv[4]});

server.on('error', (e) => {
    console.error('SERVER ERROR: %j', e);
});

server.listen(process.env.PORT || '8888');
