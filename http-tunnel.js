/*const http = require('http');
const net = require('net');
const { URL } = require('url');

// Create an HTTP tunneling proxy
const proxy = http.createServer((req, res) => {
    console.log(req.url);
});
proxy.on('connect', (req, clientSocket, head) => {
  // Connect to an origin server
  console.log(head);
  const { port, hostname } = new URL(`http://${req.url}`);
  const serverSocket = net.connect(port || 80, hostname, () => {
    
    serverSocket.write(head);
    serverSocket.pipe(clientSocket);
    clientSocket.pipe(serverSocket);
  });
});

// Now that proxy is running
proxy.listen(process.env.PORT);*/

const net = require('net');
const http = require('http');

//const server = http.createServer((req, res) => {
const server = net.createServer();

server.on('connection', (clientToProxySocket) => {
    clientToProxySocket.once('data', (data) => {
        let stringData = data.toString();
        console.log(stringData)
        let match = stringData.match(/^CONNECT\s+([^:]+):(\d+)/i)
        let isTLSConnection = Boolean(match);
        
        // By Default port is 80
        let serverPort = 80;
        let serverAddress;
        if (isTLSConnection) {
            
            // Port changed if connection is TLS
            serverPort = match[2];
            serverAddress = match[1];
        } else if((match = stringData.match(/X-Host:\s+(.+)/i))) {
            serverAddress = match[1];
            stringData = stringData.replace(/(Host:\s+|Referer:\s+).+/ig, `$1${serverAddress}`);
        } else {
            return clientToProxySocket.end();
        }

        
        
        const proxyToServerSocket = net.createConnection({host: serverAddress, port: serverPort}, () => {
            if (isTLSConnection) {
                clientToProxySocket.write('HTTP/1.1 200 OK\r\n\n');
            } else {
                proxyToServerSocket.write(stringData);
            }

            clientToProxySocket.pipe(proxyToServerSocket);
            proxyToServerSocket.pipe(clientToProxySocket);

            proxyToServerSocket.on('error', (e) => {
                console.log('proxyToServerSocket error', e);
            });
        });
        clientToProxySocket.on('error', (e) => {
            console.log('clientToProxySocket error', e);
        });
    });
});

server.on('error', (err) => {
    console.log(err);
});

server.listen(process.env.PORT || '8888', () => {
    console.log('server is running on port ' + server.address().port);
});