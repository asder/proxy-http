const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');
const zlib = require('zlib');
const stream = require('stream');
const clients = {
    http, 
    https
};
const indexHTML = fs.readFileSync(path.join(__dirname, 'index.html'));
const iconPNG = fs.readFileSync(path.join(__dirname, 'icon16.png'));
const replaceUrlScript = fs.readFileSync(path.join(__dirname, 'replace-urls.html'));


const server = http.createServer((clientReq, clientRes) => {
    const proxyHost = clientReq.headers.host;
    
    if(proxyHost.match(/^[^.]+\.[^.]+$/)) {
        if(clientReq.method === 'GET') {
            if(clientReq.url === '/') {
                clientRes.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
                clientRes.write(indexHTML);
                clientRes.end();
            } else if(clientReq.url === '/icon16.png') {
                clientRes.writeHead(200, {'Content-Type': 'image/png'});
                clientRes.write(iconPNG);
                clientRes.end();
            }
        }
        return;
    }

    const path = clientReq.url.replace(/(?:\??&?xhost|\??&?xproto)=[^&]+/ig, '');
    const urlParams = parseParams(clientReq.url || '');
    const cookieParams = parseParams(clientReq.headers.cookie || '');
    const isExtension = Boolean(clientReq.headers['x-online-proxy']);

    let host = proxyHost.replace(/\.[^.]+\.[^.]+$/, '').replace(/\-_\-/ig, '.') || cookieParams && cookieParams.host;
    let proto = clientReq.headers['x-proto'] || urlParams && urlParams.proto || cookieParams && cookieParams.proto;

    if(!proto) {
        proto = 'https';
    }
    if(!host) {
        return clientRes.end(`Could parse host params`);
    }
    
    if(clientReq.method === 'OPTIONS') {
        const headers = {
            'access-control-allow-origin': clientReq.headers.origin,
		    'access-control-allow-methods': clientReq.headers['access-control-request-method'] || '*',
            'access-control-allow-headers': clientReq.headers['access-control-request-headers'] || '*'
        }
        clientRes.writeHead(200, headers);
        return clientRes.end();
    }
    
    const options = {
        host,
        path,
        method: clientReq.method,
        headers: Object.assign({}, clientReq.headers, {
            host,
            origin: `${proto}://${host}`,
            referer: `${proto}://${host}${path}`
        })
    };
    
    delete options.headers['access-control-request-method'];
    delete options.headers['access-control-request-headers'];

    proxyRequest({proto, options, clientReq, clientRes, proxyHost, isExtension});
});

server.on('error', console.log);

server.listen(process.env.PORT, () => {
    console.log('server is running on port ' + server.address().port);
});

function proxyRequest({proto, options, clientReq, clientRes, proxyHost, isExtension}) {
    const client = clients[proto];
    const request = client.request(options, (proxyRes) => {
        const respHeaders = proxyRes.headers;
        delete respHeaders['content-security-policy-report-only'];
        respHeaders['content-security-policy'] = 'upgrade-insecure-requests';
        if(respHeaders['access-control-allow-origin'] !== '*' && clientReq.headers.origin) {
            respHeaders['access-control-allow-origin'] = clientReq.headers.origin;
        } else if(respHeaders['sec-fetch-mode'] === 'cors' && respHeaders['sec-fetch-site'] === 'cross-site') {
            respHeaders['access-control-allow-origin'] = `https://${proxyHost}`;
        }
        respHeaders['access-control-allow-credentials'] = 'true';
        respHeaders['set-cookie'] = respHeaders['set-cookie'] || [];
        respHeaders['set-cookie'].push(`xproto=${proto}; path=/; HttpOnly`);
        respHeaders['set-cookie'].forEach((str, i) => {
            respHeaders['set-cookie'][i] = str.replace(/domain=[^;]+/ig, `domain=${proxyHost}`);
        });
        
        clientRes.writeHead(proxyRes.statusCode, respHeaders);
        if(!isExtension && respHeaders['content-type'] && respHeaders['content-type'].indexOf('text/html') !== -1) {
            if(respHeaders['content-encoding']) {
                delete respHeaders['content-encoding'];
                delete respHeaders['content-length'];
                delete respHeaders['transfer-encoding'];

                let data = replaceUrlScript;
                const gunzip = zlib.createGunzip();
               
                proxyRes.pipe(gunzip);

                gunzip.on('data', (chunk) => {
                    data = Buffer.concat([data, chunk]);
                });
                
                gunzip.on('end', () => {
                    clientRes.writeHead(proxyRes.statusCode, respHeaders);
                    clientRes.write(data);
                    clientRes.end();
                });

                gunzip.on('error', console.log);
                
            } else {
                clientRes.write(replaceUrlScript);
                proxyRes.pipe(clientRes, {
                    end: true
                });
            }
        } else {
            proxyRes.pipe(clientRes, {
                end: true
            });
        }
    });

    request.on('error', (e) => {
        if(proto === 'https') {
            proxyRequest({proto: 'http', options, clientReq, clientRes, proxyHost});
        } else {
            console.log(e);
            clientRes.end('Couldn not proxy domain');
        }
    });

    request.once('socket', (socket) => {
        if(proto === 'http') {
            socket.once('connect', () => {
                clientReq.pipe(request, {
                    end: true
                });
            });
        } else if(proto === 'https') {
            socket.once('secureConnect', () => {
                clientReq.pipe(request, {
                    end: true
                });
            });
        }
    });
}

function parseParams(string) {
    const match = string.match(/(?:xhost|xproto)=[^;^&]+/ig);
    if(match) {
        let output = {};
        match.forEach((m) => {
            const [pname, value] = m.split('=');
            switch(pname) {
                case 'xhost':
                    output.host = decodeURIComponent(value);
                    break;
                case 'xproto':
                    output.proto = decodeURIComponent(value);
                    break;
            }
        });
        return output;
    }
}